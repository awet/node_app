var app = require('express')();
const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer(app);
var path = require('path');
var directories = path.dirname(__dirname);
console.log(directories); 

app.get('/', function(req, res){
  res.sendFile(directories + '/client/index.html');
});

var io = require('socket.io')(server);
io.on('connection', function(socket){
  console.log('a user connected');
  var msg = {
    'name' : 'Awet Tsegazeab',
    'msg' : 'Jesus is ahead '
  }

  socket.on('joinRoom', function(room) {
    socket.join(room);
    console.log('room join', room);
  });
  
  io.emit('server msg', msg);
});

setInterval(function(){
  var message = "The server number is: " + Math.random();
  io.sockets.to('0911003994').emit('server msg', message);
}, 5000);

setInterval(function(){
  var message = "peace and happines";
  io.sockets.to('jesus').emit('server msg', message);
}, 5000);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

